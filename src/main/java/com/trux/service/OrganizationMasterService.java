package com.trux.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trux.dao.OrganizationMasterDAO;
import com.trux.model.OrganizationMasterRegistration;
@Service
public class OrganizationMasterService  {

	@Autowired
	private OrganizationMasterDAO organizationMasterDAO;
	 
	public OrganizationMasterRegistration registerOrg(OrganizationMasterRegistration dto) {
		
		return organizationMasterDAO.registerOrg(dto);
	}

	 
	public OrganizationMasterRegistration getOrgMasterDetails(OrganizationMasterRegistration dto) {
		
		return organizationMasterDAO.getOrgMasterDetails(dto);
	}

	 
	public List<OrganizationMasterRegistration> getOrganizationMasterRegistration() {
		
		return organizationMasterDAO.getOrganizationMasterRegistration();
	}
	
	
	public OrganizationMasterRegistration getClientNameById(Integer idClientMaster) {
		return organizationMasterDAO.getClientNameById(idClientMaster);
		
	}

}
