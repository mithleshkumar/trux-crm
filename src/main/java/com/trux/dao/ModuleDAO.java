package com.trux.dao;

import java.util.List;

import com.trux.model.Cities;
import com.trux.model.Countries;
import com.trux.model.Module;
import com.trux.model.States;

public interface ModuleDAO {

	public Module save(Module dto);
	public List<Module> getModule(String accessRightsRole);
	public List<Countries> getAllCountriesList();
	public List<States> getStatesListForACountry(Integer countryId) ;
	public List<Cities> getAllCitiesForAState(Integer stateId);
	public Cities getAllCityById(Integer cityId);
	
	public States getAllStateById(Integer stateId) ;
	public Countries getAllCountryById(Integer countryID) ;
	
}
