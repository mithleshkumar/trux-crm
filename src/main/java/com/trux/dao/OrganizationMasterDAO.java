package com.trux.dao;

import java.util.List;

import com.trux.model.OrganizationMasterRegistration;

public interface OrganizationMasterDAO {

	public OrganizationMasterRegistration registerOrg(OrganizationMasterRegistration dto);
	public OrganizationMasterRegistration getOrgMasterDetails(OrganizationMasterRegistration dto);
	public List<OrganizationMasterRegistration> getOrganizationMasterRegistration();
    public OrganizationMasterRegistration getClientNameById(Integer	clientId);
	
}
