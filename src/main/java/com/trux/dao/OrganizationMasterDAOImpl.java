package com.trux.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.trux.enumerations.TruxErrorCodes;
import com.trux.model.OrganizationMasterRegistration;

public class OrganizationMasterDAOImpl  implements OrganizationMasterDAO{
private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
	return sessionFactory;
}

public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
}

	@Override
	public OrganizationMasterRegistration registerOrg(OrganizationMasterRegistration dto) {
		Session session=sessionFactory.openSession();
		try{
		Transaction tx= session.beginTransaction();
		session.save(dto);
		tx.commit();
		DetachedCriteria maxId = DetachedCriteria.forClass(OrganizationMasterRegistration.class).setProjection(Projections.max("idClientMaster"));
	    @SuppressWarnings("unchecked")
		List<OrganizationMasterRegistration>  idList=session.createCriteria(OrganizationMasterRegistration.class).add(Property.forName("idClientMaster").eq(maxId)).list();
	      session.flush();
	      session.clear();
	      session.close(); 
	      if(idList!=null && !idList.isEmpty()){ 
			return idList.get(0);
	      }else{
	    	  OrganizationMasterRegistration dtos=  new OrganizationMasterRegistration();
	    	   dtos.setErrorCode(TruxErrorCodes.DRIVER_COLLECTION_CREATION_ERROR_MESSAGE.getCode());
	    	   dtos.setErrorMessage(TruxErrorCodes.DRIVER_COLLECTION_CREATION_ERROR_MESSAGE.getDescription());
	    	  return dtos;
	      }
	      }catch(Exception er){
	    	  er.printStackTrace();
	    	  session.close();
	      }
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrganizationMasterRegistration getOrgMasterDetails(OrganizationMasterRegistration dto) {
		Session session=sessionFactory.openSession();
		try{
		List<OrganizationMasterRegistration> omrList = session.createQuery("from OrganizationMasterRegistration d where  d.name='"+dto.getName()+"'  order by name").list();
        session.close();
        if(omrList!=null && !omrList.isEmpty()){
        	return omrList.get(0);
        }
}catch(Exception er){
	er.printStackTrace();
	session.close();
}
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrganizationMasterRegistration> getOrganizationMasterRegistration() {
		Session session=sessionFactory.openSession();
		try{
		List<OrganizationMasterRegistration> omrList = session.createQuery("from OrganizationMasterRegistration d order by name").list();
        session.close();
        if(omrList!=null && !omrList.isEmpty()){
        	return omrList;
        }
         }catch(Exception er){
        	 er.printStackTrace();
        	 session.close();
         }
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrganizationMasterRegistration getClientNameById(Integer idClientMaster) {
		Session session=sessionFactory.openSession();
		try{
		List<OrganizationMasterRegistration> omrList = session.createQuery("from OrganizationMasterRegistration d where d.idClientMaster="+idClientMaster+" order by name").list();
        session.close();
        if(omrList!=null && !omrList.isEmpty()){
        	return omrList.get(0);
        }
         }catch(Exception er){
        	 er.printStackTrace();
        	 session.close();
         }
		return null;
	}

}
