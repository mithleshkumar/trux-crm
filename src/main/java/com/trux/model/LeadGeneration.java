package com.trux.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "lead_generation")
public class LeadGeneration {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private Integer agentId;
	@Column(name = "imageOfVisitingCard")
	private String imageOfVisitingCard;
	@Column(name = "comments")
	private String comments;
	@Column(name = "scheduledMeetingDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduledMeetingDate;
	@Column(name = "scheduledMeetingTime") 
	private String scheduledMeetingTime;
	@Column(name = "scheduledMeetingTitle")
	private String scheduledMeetingTitle;
	@Column(name = "sendProposal")
	private String sendProposal;
	@Column(name = "loginAgentId")
	private String loginAgentId;
	@Transient
	private String errorCode;	
	@Transient
	private String errorMessage;
	public LeadGeneration() {

	}

	public LeadGeneration(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getLoginAgentId() {
		return loginAgentId;
	}

	public void setLoginAgentId(String loginAgentId) {
		this.loginAgentId = loginAgentId;
	}

	public LeadGeneration(String imageOfVisitingCard, String comments,
			Date scheduledMeetingDate, String scheduledMeetingTime,
			String scheduledMeetingTitle, String sendProposal,
			String loginAgentId) {
		super();
		this.imageOfVisitingCard = imageOfVisitingCard;
		this.comments = comments;
		this.scheduledMeetingDate = scheduledMeetingDate;
		this.scheduledMeetingTime = scheduledMeetingTime;
		this.scheduledMeetingTitle = scheduledMeetingTitle;
		this.sendProposal = sendProposal;
		this.loginAgentId = loginAgentId; 
	}

	public LeadGeneration(String imageOfVisitingCard, String comments,
			Date scheduledMeetingDate, String scheduledMeetingTime,
			String scheduledMeetingTitle, String sendProposal) {
		this.imageOfVisitingCard = imageOfVisitingCard;
		this.comments = comments;
		this.scheduledMeetingDate = scheduledMeetingDate;
		this.scheduledMeetingTime = scheduledMeetingTime;
		this.scheduledMeetingTitle = scheduledMeetingTitle;
		this.sendProposal = sendProposal;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public String getImageOfVisitingCard() {
		return imageOfVisitingCard;
	}

	public void setImageOfVisitingCard(String imageOfVisitingCard) {
		this.imageOfVisitingCard = imageOfVisitingCard;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getScheduledMeetingDate() {
		return scheduledMeetingDate;
	}

	public void setScheduledMeetingDate(Date scheduledMeetingDate) {
		this.scheduledMeetingDate = scheduledMeetingDate;
	}

	public String getScheduledMeetingTime() {
		return scheduledMeetingTime;
	}

	public void setScheduledMeetingTime(String scheduledMeetingTime) {
		this.scheduledMeetingTime = scheduledMeetingTime;
	}

	public String getScheduledMeetingTitle() {
		return scheduledMeetingTitle;
	}

	public void setScheduledMeetingTitle(String scheduledMeetingTitle) {
		this.scheduledMeetingTitle = scheduledMeetingTitle;
	}

	public String getSendProposal() {
		return sendProposal;
	}

	public void setSendProposal(String sendProposal) {
		this.sendProposal = sendProposal;
	}

}
